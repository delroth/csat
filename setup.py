from csat import VERSION
from setuptools import setup, find_packages

setup(
    name = "CSAT",
    version = VERSION,
    packages = find_packages(),
    scripts = [
        'csat/scripts/csat',
    ],
)
