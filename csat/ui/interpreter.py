import csat
import csat.db
from csat.ui import extensions
import sys
from lexy import (
    BaseLexer, LexingError,
    String,
    newline, integer, identifier, string
)
import os.path
import pickle


class Lexer(BaseLexer):
    yield_newlines = True

    def __next__(self):
        next_token = super(Lexer, self).__next__()
        if next_token == identifier:
            next_token = String(next_token.value, next_token.location)
        return next_token


class Interpreter:

    def __init__(self, db_file=None):
        self.binary_loaders = {}
        for binfmt in extensions.get_binfmt():
            for variant in binfmt.variants:
                self.binary_loaders[variant] = binfmt

        self.disassemblers = {}
        for disas in extensions.get_disas():
            for variant in disas.variants:
                self.disassemblers[variant] = disas

        self.db_file = db_file
        self.db = csat.db.Database()
        if db_file:
            # TODO: create a real file format. Using pickle is not secure!
            try:
                with open(db_file, 'rb') as f:
                    self.db = pickle.load(f)
            except (OSError, IOError) as e:
                print('Cannot read database file:', e)
            except (ValueError, EOFError) as e:
                print('Invalid database file:', e)
        self.lexer = Lexer(sys.stdin)

    def readline(self):
        '''
        Read and return a line of tokens.
        '''
        tokens = []
        valid_line = True
        while True:
            try:
                tokens.append(next(self.lexer))
            except LexingError as e:
                print('Invalid token:', e)
                valid_line = False
            else:
                if tokens[-1] == newline:
                    break
        if not valid_line:
            return []
        tokens.pop()
        return tokens

    def _dispatch(self, command):
        '''
        Return the callback associated to a given command word, or None if
        there is no such command.
        '''
        return getattr(self, 'cmd_{}'.format(command), None)

    def cmd_help(self, tokens):
        '''
        Display some help on various topics.
        '''
        if not tokens:
            print('Display help on various topics:')
            print('    binfmt  Print the list of binary loaders')
            print('    disas   Print the list of disassemblers')
            print('    <cmd>   Print the purpose of a command')
        elif len(tokens) == 1 and tokens[0] == string:
            token = tokens[0]
            if token == String('binfmt'):
                # Only print the keys who are the first variant of the binary
                # loader.
                for name, binfmt in self.binary_loaders.items():
                    if name != binfmt.variants[0]:
                        continue
                    print('{} {} ({})'.format(
                        name.ljust(16), binfmt.name, ', '.join(binfmt.variants)
                    ))
            elif token == String('disas'):
                # Only print the keys who are the first variant of the
                # disassembler.
                for name, disas in self.disassemblers.items():
                    if name != disas.variants[0]:
                        continue
                    print('{} {} ({})'.format(
                        name.ljust(16), disas.name, ', '.join(disas.variants)
                    ))
            elif self._dispatch(token.value):
                callback = self._dispatch(token.value)
                print('{}: {}'.format(
                    token.value, callback.__doc__.strip().split('\n')[0]
                ))
            else:
                print('Invalid topic:', token.value)
                return False
        else:
            print('Invalid arguments')
            return False
        return True

    def cmd_save(self, tokens):
        '''
        Save the database to a file.
        '''
        if len(tokens) == 0:
            if self.db_file is None:
                print('A filename is required')
                return False
            db_file = self.db_file
        elif len(tokens) == 1 and tokens[0] == string:
            db_file = tokens[0].value
        else:
            print('Invalid arguments')
            print('Usage: save [file]')
            return False

        try:
            with open(db_file, 'wb') as f:
                pickle.dump(self.db, f)
        except (OSError, IOError) as e:
            print('Error while writing in {}: {}'.format(db_file, e))
            return False
        else:
            self.db_file = db_file
            return True

    def cmd_load(self, tokens):
        '''
        Load some binary to the database.
        '''
        if any(token != string for token in tokens):
            print('Invalid arguments (names only!)')
            return False

        if len(tokens) not in (1, 2):
            print('Invalid number of arguments')
            print('Usage: load [binfmt] filename')
            return False

        if len(tokens) == 1:
            filename = tokens[0].value
        else:
            filename = tokens[1].value

        try:
            fp = open(os.path.expanduser(filename), 'rb')
        except (OSError, IOError) as e:
            print('Cannot open {}: {}'.format(filename, e))
            return False

        if len(tokens) == 1:
            for BinaryLoader in self.binary_loaders.values():
                binfmt = BinaryLoader()
                fp.seek(0)
                if binfmt.accept(fp, filename):
                    fp.seek(0)
                    binfmt.load(fp, self.db)
                    break
            else:
                print('No binary loader could load', filename)
        else:
            try:
                binfmt = self.binary_loaders[tokens[0].value]()
            except KeyError:
                print('Unknown binfmt:', tokens[0].value)
                return False
            if not binfmt.accept(fp, filename):
                print('The binary loader cannot load', filename)
                return False
            fp.seek(0)
            binfmt.load(fp, self.db)
        fp.close()

        arch = self.db.architecture
        if arch:
            try:
                disas = self.disassemblers[arch](self.db)
            except KeyError:
                print('Unknown disas:', arch)
                return False
            disas.disassemble()
        else:
            print('The binary loader could not determine the architecture')

        return True

    def cmd_label(self, tokens):
        '''
        Display or set labels.
        '''
        if len(tokens) == 0:
            addresses = sorted(self.db.labels)
            for addr in addresses:
                name = self.db.labels[addr]
                print('{}\t{}'.format(
                    self.db.fmt_addr(addr, label=False), name
                ))
            return True

        elif len(tokens) == 2:
            if tokens[0] == string and tokens[1] == integer:
                self.db.set_label(tokens[0].value, tokens[1].value)
                return True
            elif tokens[0] == String('rm') and tokens[1] == string:
                if not self.db.del_label(tokens[1].value):
                    print('No such label:', tokens[1].value)
                    return False
                return True

        print('Invalid arguments')
        print('Usage: label (display labels)')
        print('       label name address (set a label)')
        print('       label rm name (remove a label)')
        return False

    def cmd_show(self, tokens):
        '''
        Display some information from the database.
        '''
        if len(tokens) == 0:
            print(self.db)
        elif len(tokens) == 1:
            if tokens[0] == String('info'):
                print(self.db.format(include_bblks=False))
            else:
                if tokens[0] == integer:
                    address = tokens[0].value
                else:
                    try:
                        address = self.db.label_to_addr[tokens[0].value]
                    except KeyError:
                        print('Unknown label:', tokens[0].value)
                        return False
                bblks = self.db.find_bblks(address)
                if bblks:
                    print(self.db._str_bblks(bblks))
                else:
                    print('No basic block at this place')
        elif len(tokens) != 1:
            print('Invalid arguments')
            print('Usage: show (display the whole database')
            print('       show info (display metadata and segments')
            print('       show address (display basic blocks around one '
                  'address)')
            return False
        return True

    def cmd_exit(self, tokens):
        '''
        Stop the session.
        '''
        if tokens:
            print('exit takes no argument')
            return False
        self.is_looping = False
        return True

    def loop(self):
        print('Csat version {}'.format(csat.VERSION))
        print('Interactive command-line tool')

        self.is_looping = True
        while self.is_looping:

            # Print prompt
            print('>', end=' ')
            sys.stdout.flush()

            # Get a line of tokens
            try:
                tokens = self.readline()
            except StopIteration:
                print('')
                break
            if not tokens:
                continue

            # And try to execute the corresponding command
            callback = self._dispatch(tokens[0].value)
            if callback:
                callback(tokens[1:])
            else:
                print('Unknown command:', tokens[0].value)
        print('')
