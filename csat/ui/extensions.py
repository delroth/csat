import csat.binfmt
import csat.disas
import imp
import os
import sys

user_prefix = os.path.join(os.path.expanduser('~'), '.csat')
import_paths = {
    'binfmt': [
        os.path.join(user_prefix, 'binfmt'),
        os.path.dirname(os.path.abspath(csat.binfmt.__file__)),
    ],
    'disas': [
        os.path.join(user_prefix, 'disas'),
        os.path.dirname(os.path.abspath(csat.disas.__file__)),
    ],
}

def get_extensions(ext_type):
    '''
    Try to import as many extensions as possible from system and user import
    paths for a given extension type.
    '''
    extensions = {}
    for path in import_paths[ext_type]:
        try:
            ext_files = os.listdir(path)
        except OSError:
            continue
        for ext in ext_files:
            module_name = ext.rsplit('.', 1)[0]
            if module_name in extensions:
                continue

            try:
                file, pathname, desc = imp.find_module(module_name, [path])
            except ImportError as e:
                continue

            try:
                module = imp.load_module(
                    'csat.extensions.{}.{}'.format(ext_type, module_name),
                    file, pathname, desc
                )
            except ImportError as e:
                continue
            else:
                extensions[module_name] = module

    return extensions

def get_binfmt():
    '''
    Return a list of as many imported binary loaders as possible.
    '''
    return [
        ext.BinaryLoader for ext in get_extensions('binfmt').values()
        if hasattr(ext, 'BinaryLoader')
    ]

def get_disas():
    '''
    Return a list of as many imported disassemblers as possible.
    '''
    return [
        ext.Disassembler for ext in get_extensions('disas').values()
        if hasattr(ext, 'Disassembler')
    ]
