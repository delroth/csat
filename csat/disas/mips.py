from csat.db.utils import LITTLE_ENDIAN, BIG_ENDIAN
from csat.disas.base import RecursiveDisassembler
from csat.disas.utils import *
import struct

REGISTERS = [
    Reg(name="r0", size=4),
    Reg(name="at", size=4),
    Reg(name="v0", size=4),
    Reg(name="v1", size=4),
    Reg(name="a0", size=4),
    Reg(name="a1", size=4),
    Reg(name="a2", size=4),
    Reg(name="a3", size=4),
    Reg(name="t0", size=4),
    Reg(name="t1", size=4),
    Reg(name="t2", size=4),
    Reg(name="t3", size=4),
    Reg(name="t4", size=4),
    Reg(name="t5", size=4),
    Reg(name="t6", size=4),
    Reg(name="t7", size=4),
    Reg(name="s0", size=4),
    Reg(name="s1", size=4),
    Reg(name="s2", size=4),
    Reg(name="s3", size=4),
    Reg(name="s4", size=4),
    Reg(name="s5", size=4),
    Reg(name="s6", size=4),
    Reg(name="s7", size=4),
    Reg(name="t8", size=4),
    Reg(name="t9", size=4),
    Reg(name="k0", size=4),
    Reg(name="k1", size=4),
    Reg(name="gp", size=4),
    Reg(name="sp", size=4),
    Reg(name="fp", size=4),
    Reg(name="ra", size=4),
]

_Instr = Instr
def Instr(**kwargs):
    if kwargs['flow_type'] in (JUMP, BRANCH, CALL):
        try:
            kwargs['flow_dest'] = [
                op for op in kwargs['operands']
                if op.type in (REG_CODE_NEAR, CODE_NEAR)
            ][0]
        except IndexError:
            # If there is no code reference, then this is not a flow control
            # instruction!
            raise ValueError('There is no destination for this flow instruction')
    else:
        kwargs['flow_dest'] = None
    return _Instr(**kwargs)

class BDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        off = opcode.int(0, 16) * 4
        rs = REGISTERS[opcode.uint(21, 26)]
        return Instr(
            name=self.name, address=pc, size=4,
            operands=[
                Operand(type=REG, value=rs),
                Operand(type=CODE_NEAR, value=pc + off),
            ],
            flow_type=getattr(self, 'flow_type', None)
        )

class IDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        if hasattr(self, "signed"):
            imm = opcode.int(0, 16)
            imm_type = IMMEDIATE
        elif hasattr(self, "flow_type"):
            imm = pc + opcode.int(0, 16) * 4
            imm_type = CODE_NEAR
        else:
            imm = opcode.uint(0, 16)
            imm_type = IMMEDIATE

        rt = REGISTERS[opcode.uint(16, 21)]
        rs = REGISTERS[opcode.uint(21, 26)]

        rs = Operand(type=REG, value=rs)
        rt = Operand(type=REG, value=rt)
        imm = Operand(type=imm_type, value=imm)

        shift = getattr(self, "shift", None)
        if shift == 'rs':
            operands = [rt, rs._replace(shift=imm)]
        elif shift == 'rt':
            operands = [rs, rt._replace(shift=imm)]
        else:
            operands = [rs, rt, imm]

        return Instr(
            name=self.name, address=pc, size=4,
            operands=operands,
            flow_type=getattr(self, 'flow_type', None)
        )

class JDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        target = (pc & 0xF0000000) | (opcode.uint(0, 26) * 4)
        return Instr(
            name=self.name, address=pc, size=4,
            operands=[Operand(type=CODE_NEAR, value=target)],
            flow_type=getattr(self, 'flow_type', None)
        )

class RDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        shift = opcode.uint(6, 11)
        rd = REGISTERS[opcode.uint(11, 16)]
        rt = REGISTERS[opcode.uint(16, 21)]
        rs = REGISTERS[opcode.uint(21, 26)]

        operands = []
        if hasattr(self, "rd"):
            operands.append(Operand(type=self.rd, value=rd))
        if hasattr(self, "rs"):
            operands.append(Operand(type=self.rs, value=rs))
        if hasattr(self, "rt"):
            operands.append(Operand(type=self.rt, value=rt))
        if hasattr(self, "shift"):
            operands.append(Operand(type=self.shift, value=shift))

        return Instr(
            name=self.name, address=pc, size=4, operands=operands,
            flow_type=getattr(self, 'flow_type', None)
        )

SPECIAL_DECODERS = FixedSizeDispatcher(bits=(0, 6), match={
    0:  RDecoder(name="sll", rd=REG, rt=REG, shift=IMMEDIATE),
    2:  RDecoder(name="srl", rd=REG, rt=REG, shift=IMMEDIATE),
    3:  RDecoder(name="sra", rd=REG, rt=REG, shift=IMMEDIATE),
    4:  RDecoder(name="sllv", rd=REG, rt=REG, rs=REG),
    6:  RDecoder(name="srlv", rd=REG, rt=REG, rs=REG),
    7:  RDecoder(name="srav", rd=REG, rt=REG, rs=REG),
    8:  RDecoder(name="jr", rs=REG_CODE_NEAR, flow_type=JUMP),
    9:  RDecoder(name="jalr", rs=REG_CODE_NEAR, flow_type=CALL),
    12: RDecoder(name="syscall"),
    13: RDecoder(name="break"),
    16: RDecoder(name="mfhi", rd=REG),
    17: RDecoder(name="mthi", rs=REG),
    18: RDecoder(name="mflo", rd=REG),
    19: RDecoder(name="mtlo", rs=REG),
    24: RDecoder(name="mult", rs=REG, rt=REG),
    25: RDecoder(name="multu", rs=REG, rt=REG),
    26: RDecoder(name="div", rs=REG, rt=REG),
    27: RDecoder(name="divu", rs=REG, rt=REG),
    32: RDecoder(name="add", rd=REG, rs=REG, rt=REG),
    33: RDecoder(name="addu", rd=REG, rs=REG, rt=REG),
    34: RDecoder(name="sub", rd=REG, rs=REG, rt=REG),
    35: RDecoder(name="subu", rd=REG, rs=REG, rt=REG),
    36: RDecoder(name="and", rd=REG, rs=REG, rt=REG),
    37: RDecoder(name="or", rd=REG, rs=REG, rt=REG),
    38: RDecoder(name="xor", rd=REG, rs=REG, rt=REG),
    39: RDecoder(name="nor", rd=REG, rs=REG, rt=REG),
    42: RDecoder(name="slt", rd=REG, rs=REG, rt=REG),
    43: RDecoder(name="sltu", rd=REG, rs=REG, rt=REG),
})

BRANCH_DECODERS = FixedSizeDispatcher(bits=(16, 21), match={
    0:  BDecoder(name="bltz", flow_type=BRANCH),
    1:  BDecoder(name="bgez", flow_type=BRANCH),
    16: BDecoder(name="bltzal", flow_type=BRANCH),
    17: BDecoder(name="bgezal", flow_type=BRANCH),
})

DECODERS = FixedSizeDispatcher(bits=(26, 32), match={
    0:  SPECIAL_DECODERS,
    1:  BRANCH_DECODERS,
    2:  JDecoder(name="j", flow_type=JUMP),
    3:  JDecoder(name="jal", flow_type=CALL),
    4:  IDecoder(name="beq", flow_type=BRANCH),
    5:  IDecoder(name="bne", flow_tybe=BRANCH),
    6:  BDecoder(name="blez", flow_type=BRANCH),
    7:  BDecoder(name="bgtz", flow_type=BRANCH),
    8:  IDecoder(name="addi", signed=True),
    9:  IDecoder(name="addiu"),
    10: IDecoder(name="slti", signed=True),
    11: IDecoder(name="sltiu"),
    12: IDecoder(name="andi"),
    13: IDecoder(name="ori"),
    14: IDecoder(name="xori"),
    15: IDecoder(name="lui"),
    32: IDecoder(name="lb", signed=True, shift="rs"),
    33: IDecoder(name="lh", signed=True, shift="rs"),
    34: IDecoder(name="lwl", signed=True, shift="rs"),
    35: IDecoder(name="lw", signed=True, shift="rs"),
    36: IDecoder(name="lbu", signed=True, shift="rs"),
    37: IDecoder(name="lhu", signed=True, shift="rs"),
    38: IDecoder(name="lwr", signed=True, shift="rs"),
    40: IDecoder(name="sb", signed=True, shift="rs"),
    41: IDecoder(name="sh", signed=True, shift="rs"),
    42: IDecoder(name="swl", signed=True, shift="rs"),
    43: IDecoder(name="sw", signed=True, shift="rs"),
    46: IDecoder(name="swr", signed=True, shift="rs"),
})

class Disassembler(RecursiveDisassembler):
    name = "MIPS32"
    variants = [
        "mips",
        "mips32",
        "mips_r3000"
    ]
    delay_slots = 1

    registers = REGISTERS
    opcode_le = struct.Struct('<I')
    opcode_be = struct.Struct('>I')

    def __init__(self, *args, **kwargs):
        super(Disassembler, self).__init__(*args, **kwargs)
        self.opcode_struct = (
            self.opcode_le
            if self.db.endianess == LITTLE_ENDIAN else
            self.opcode_be
        )

    def decode(self):
        pc = self.pc
        opcode = BitField(self.opcode_struct.unpack(self.read(4))[0])
        decoder = DECODERS.dispatch(opcode)
        if decoder is None:
            return None
        return decoder(opcode, pc)
