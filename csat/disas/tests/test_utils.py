from csat.disas.utils import BitField, FixedSizeDispatcher

import unittest

class TestBitField(unittest.TestCase):
    def test_uint(self):
        """Test unsigned int extraction from a bitfield"""
        self.assertEqual(BitField(0x12345678).uint(8, 16), 0x56)
        self.assertEqual(BitField(0x12345678).uint(0, 32), 0x12345678)
        self.assertEqual(BitField(0x12345678).uint(4, 8), 0x7)
        self.assertEqual(BitField(0x12345678).uint(0, 16), 0x5678)
        self.assertEqual(BitField(0x12345678).uint(24, 32), 0x12)
        self.assertEqual(BitField(0x12345678).uint(16, 32), 0x1234)

    def test_int(self):
        """Test signed int extraction from a bitfield"""
        self.assertEqual(BitField(0x12345678).int(8, 16), 0x56)
        self.assertEqual(BitField(0x12345678).int(0, 32), 0x12345678)
        self.assertEqual(BitField(0x12345678).int(4, 8), 0x7)
        self.assertEqual(BitField(0x12345678).int(0, 16), 0x5678)
        self.assertEqual(BitField(0x12345678).int(24, 32), 0x12)
        self.assertEqual(BitField(0x12345678).int(16, 32), 0x1234)

        self.assertEqual(BitField(0x89ABCDEF).int(8, 16), -51)
        self.assertEqual(BitField(0x89ABCDEF).int(0, 32), -1985229329)
        self.assertEqual(BitField(0x89ABCDEF).int(4, 8), -2)
        self.assertEqual(BitField(0x89ABCDEF).int(0, 16), -12817)
        self.assertEqual(BitField(0x89ABCDEF).int(24, 32), -119)
        self.assertEqual(BitField(0x89ABCDEF).int(16, 32), -30293)


class TestFixedSizeDispatcher(unittest.TestCase):
    def test_simple(self):
        """Test simple, non hierarchic, fixed size dispatch"""
        dispatcher = FixedSizeDispatcher((8, 12), {
            0: "zero",
            1: "one",
            2: "two",
        })

        self.assertEqual(dispatcher.dispatch(0x10FF), "zero")
        self.assertEqual(dispatcher.dispatch(0x11FF), "one")
        self.assertEqual(dispatcher.dispatch(0x12FF), "two")
        self.assertEqual(dispatcher.dispatch(0x13FF), None)

    def test_hierarchic(self):
        """Test several hierarchic fixed size dispatchers"""
        dispatcher1 = FixedSizeDispatcher((8, 12), {
            0: "1_zero",
            1: "1_one",
            2: "1_two",
        })

        dispatcher2 = FixedSizeDispatcher((12, 16), {
            0: "2_zero",
            1: "2_one",
            2: "2_two",
        })

        dispatcher = FixedSizeDispatcher((0, 8), {
            1: dispatcher1,
            2: dispatcher2,
            3: "three",
        })

        self.assertEqual(dispatcher.dispatch(0x0000), None)
        self.assertEqual(dispatcher.dispatch(0x1201), "1_two")
        self.assertEqual(dispatcher.dispatch(0x1301), None)
        self.assertEqual(dispatcher.dispatch(0x2102), "2_two")
        self.assertEqual(dispatcher.dispatch(0x1202), "2_one")
        self.assertEqual(dispatcher.dispatch(0x1001), "1_zero")
        self.assertEqual(dispatcher.dispatch(0x2103), "three")
