"""
csat.disas.utils
~~~~~~~~~~~~~~~~

Utility classes to write custom disassembler modules.
"""

from collections import namedtuple
import io

def defaultnamedtuple(name, *required, **defaults):
    """
    Return a namedtuple that have default values.
    """
    all_fields = required + tuple(defaults)
    cls = namedtuple(name, all_fields)

    _new = cls.__new__
    def new(cls, *f, **fields):
        args = dict(defaults)
        args.update(dict(zip(all_fields, f)))
        args.update(fields)
        return _new(cls, **args)
    cls.__new__ = new

    return cls

class BitField:
    """Interpret an integer as a bit field and extract signed or unsigned
    integers from a bit range."""

    def __init__(self, n):
        self.n = n

    def int(self, bs, be):
        """Returns a signed int from bit bs to bit be."""
        i = self.uint(bs, be)
        if i & (1 << (be - bs - 1)):
            i -= (1 << (be - bs))
        return i

    def uint(self, bs, be):
        """Returns an unsigned int from bit bs to bit be."""
        max_mask = (1 << be) - 1
        rejected_mask = (1 << bs) - 1
        mask = max_mask & ~rejected_mask
        extracted = self.n & mask
        return extracted >> bs

class BaseInstructionDecoder:
    """Base class of an instruction decoder, which is a callable object which
    returns an instruction class from the opcode and the current program
    counter."""

    def __init__(self, **kwargs):
        """Stores the optional metadata which are given in the constructor."""
        self.__dict__.update(kwargs)

    def __call__(self, *args, **kwargs):
        raise NotImplementedError("BaseInstructionDecoder.__call__")

class FixedSizeDispatcher:
    """Match bits from an opcode against several possibilities."""

    def __init__(self, bits, match):
        self.bits = bits
        self.match = match

    def dispatch(self, opcode):
        try:
            opcode.uint
        except AttributeError:
            opcode = BitField(opcode)

        match_part = opcode.uint(*self.bits)
        matched = self.match.get(match_part, None)
        if matched is None:
            return None
        elif hasattr(matched, "dispatch"):
            return matched.dispatch(opcode)
        else:
            return matched

Reg = namedtuple("Reg", "name size")
Operand = defaultnamedtuple("Operand",
    "type", "value",
    shift=None, segment=None,
)

Instr = namedtuple(
    "Instr",
    "name address size operands "
    "flow_type flow_dest"
)

# prev: set of addresses that can jump to this basic block.
# next: set of addresses on which this basic block can jump.
BasicBlock = namedtuple("BasicBlock", "address size instrs prev next")

# Instructions' embedded value types
REG, CODE_NEAR, REG_CODE_NEAR, DATA, REG_DATA, IMMEDIATE = range(6)

# Types of flow control instruction
JUMP, BRANCH, CALL, RETURN, LOOP = range(5)

UNKNOWN = 'unknown'
CONTINUE = 'continue'

def repr_instr_full(instr, db):
    """Return a pretty-printed instruction including raw bytes and address."""
    lines = []
    address = db.fmt_addr(instr.address)
    indent = 19 # 0x, 16 hexadecimal digits and a space
    padding = ' ' * (indent - len(address))
    bytes = repr_bytes(
        db.vmem[instr.address:instr.address + instr.size]
    )
    asm = repr_instr(instr, db)
    lines.append('{}{}{}{}'.format(address, padding, bytes.pop(0), asm))
    lines.extend((
        '{}{}'.format(indent, line)
        for line in bytes
    ))
    return lines

def repr_instr(instr, db):
    """Return a pretty-printed instruction."""
    return '{} {}'.format(
        instr.name,
        ', '.join(repr_op(op, db) for op in instr.operands)
    )

def repr_op(op, db):
    """Return a pretty-printed instruction operand."""
    result = {
        REG: (lambda: '%' + op.value.name),
        REG_CODE_NEAR: (lambda: '%' + op.value.name),
        REG_DATA: (lambda: '[%' + op.value.name + ']'),
        CODE_NEAR: (lambda: db.fmt_addr(op.value)),
        DATA: (lambda: '[' + db.fmt_addr(op.value) + ']'),
        IMMEDIATE: (lambda: '{:#x}'.format(op.value)),
    }[op.type]()
    if op.segment:
        result = '{}:{}'.format(repr_op(op.segment, db), result)
    if op.shift:
        result = '{}({})'.format(repr_op(op.shift, db), result)
    return result

def repr_bytes(bytes):
    """
    Return a pretty-printed hexadecimal bytes dump.

    The result is a list of lines (a line is a mere string). There is at most 8
    bytes per line, and lines are padded with whitespaces.
    """

    lines = []
    bytes = io.BytesIO(bytes)
    empty_line = False
    while not empty_line:
        empty_line = True
        current_line = ''
        for i in range(8):
            byte = bytes.read(1)
            if i == 4:
                current_line += ' '
            if len(byte) < 1:
                current_line += '   '
            else:
                empty_line = False
                current_line += '{:02x} '.format(byte[0])
        lines.append(current_line)

    if len(lines) > 1 and not lines[-1].strip():
        lines.pop(-1)

    return lines
