from collections import namedtuple
from csat.db.utils import LITTLE_ENDIAN, BIG_ENDIAN
from csat.disas.base import RecursiveDisassembler
from csat.disas.utils import *
import struct

REGISTERS = [
    Reg(name='v0', size=1),
    Reg(name='v1', size=1),
    Reg(name='v2', size=1),
    Reg(name='v3', size=1),
    Reg(name='v4', size=1),
    Reg(name='v5', size=1),
    Reg(name='v6', size=1),
    Reg(name='v7', size=1),
    Reg(name='v8', size=1),
    Reg(name='v9', size=1),
    Reg(name='va', size=1),
    Reg(name='vb', size=1),
    Reg(name='vc', size=1),
    Reg(name='vd', size=1),
    Reg(name='ve', size=1),
    Reg(name='vf', size=1),
    Reg(name='i', size=1),  # Data pointer
    Reg(name='pc', size=2), # Program counter
    Reg(name='dt', size=8), # Delay timer
    Reg(name='st', size=8), # Sound timer
    Reg(name='k', size=8),  # Key pseudo-register
    Reg(name='f', size=8),  # Font pseudo-register
    Reg(name='b', size=8),  # BCD pseudo-register
]

I = REGISTERS[16]
PC = REGISTERS[17]
DT= REGISTERS[18]
ST = REGISTERS[19]
K = REGISTERS[20]
F = REGISTERS[21]
B = REGISTERS[22]

_Instr = Instr
def Instr(**kwargs):
    if kwargs.pop('skip', False):
        kwargs['flow_type'] = BRANCH
        kwargs['flow_dest'] = Operand(
            type=CODE_NEAR, value=kwargs['address'] + 2 * kwargs['size']
        )
    elif kwargs['flow_type'] in (JUMP, BRANCH, CALL):
        try:
            kwargs['flow_dest'] = [
                op for op in kwargs['operands']
                if op.type in (REG_CODE_NEAR, CODE_NEAR)
            ][0]
        except IndexError:
            raise ValueError(
                'There is no destination for this flow instruction'
            )
    else:
        kwargs['flow_dest'] = None
    return _Instr(**kwargs)

class AddressDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        addr = opcode.uint(0, 12)
        return Instr(
            name=self.name, address=pc, size=2,
            operands=[
                Operand(type=getattr(self, 'addr', CODE_NEAR), value=addr),
            ],
            flow_type=getattr(self, 'flow_type', None),
            skip=getattr(self, 'skip', None),
        )

class ZeroDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        switch = opcode.uint(0, 12)
        if switch == 0x0e0:
            return Instr(
                name='cls', address=pc, size=2, operands=[],
                flow_type=None, skip=False
            )
        elif switch == 0x0ee:
            return Instr(
                name='ret', address=pc, size=2, operands=[],
                flow_type=RETURN, skip=False
            )
        else:
            return Instr(
                name='sys', address=pc, size=2,
                operands=[Operand(type=CODE_NEAR, value=switch)],
                flow_type=CALL,
                skip=getattr(self, 'skip', None),
            )

class OneRegisterDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        operands = []
        try:
            operands.append(Operand(type=REG, value=self.dst))
        except AttributeError:
            pass
        try:
            operands.append(Operand(type=REG_DATA, value=self.memdst))
        except AttributeError:
            pass
        operands.append(
            Operand(type=REG, value=REGISTERS[opcode.uint(8, 12)])
        )
        try:
            operands.append(Operand(type=REG, value=self.src))
        except AttributeError:
            pass
        try:
            operands.append(Operand(type=REG_DATA, value=self.memsrc))
        except AttributeError:
            pass
        if getattr(self, 'const', False):
            operands.append(Operand(type=IMMEDIATE, value=opcode.uint(0, 8)))
        return Instr(
            name=self.name, address=pc, size=2,
            operands=operands,
            flow_type=getattr(self, 'flow_type', None),
            skip=getattr(self, 'skip', None),
        )

class ConstantDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        operands = []
        try:
            operands.append(Operand(type=REG, value=self.dst))
        except AttributeError:
            pass
        value = opcode.uint(0, 12)
        operands.append(Operand(type=IMMEDIATE, value=value))
        return Instr(
            name=self.name, address=pc, size=2, operands=operands,
            flow_type=getattr(self, 'skip', None),
            skip=getattr(self, 'skip', None),
        )

class ConstantDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        operands = []
        try:
            operands.append(Operand(type=REG, value=self.dst))
        except AttributeError:
            pass
        value = opcode.uint(0, 12)
        operands.append(Operand(type=IMMEDIATE, value=value))
        return Instr(
            name=self.name, address=pc, size=2, operands=operands,
            flow_type=getattr(self, 'flow_type', None),
            skip=getattr(self, 'skip', None),
        )

class TwoRegistersDecoder(BaseInstructionDecoder):
    def __call__(self, opcode, pc):
        operands = [
            Operand(type=REG, value=REGISTERS[opcode.uint(8, 12)]),
            Operand(type=REG, value=REGISTERS[opcode.uint(4, 8)]),
        ]
        if getattr(self, 'const', False):
            operands.append(Operand(type=IMMEDIATE, value=opcode.uint(0, 4)))
        return Instr(
            name=self.name, address=pc, size=2, operands=operands,
            flow_type=getattr(self, 'flow_type', None),
            skip=getattr(self, 'skip', None),
        )

ARITHM_DECODERS = FixedSizeDispatcher(bits=(0, 4), match={
    0x0:    TwoRegistersDecoder(name='ld'),
    0x1:    TwoRegistersDecoder(name='or'),
    0x2:    TwoRegistersDecoder(name='and'),
    0x3:    TwoRegistersDecoder(name='xor'),
    0x4:    TwoRegistersDecoder(name='add'),
    0x5:    TwoRegistersDecoder(name='sub'),
    0x6:    TwoRegistersDecoder(name='shr'),
    0x7:    TwoRegistersDecoder(name='subn'),
    0xe:    TwoRegistersDecoder(name='shl'),
})

INPUT_DECODERS = FixedSizeDispatcher(bits=(0, 8), match={
    0x9e:   OneRegisterDecoder(name='skp'),
    0xa1:   OneRegisterDecoder(name='sknp'),
})

SPECIAL_DECODERS = FixedSizeDispatcher(bits=(0, 8), match={
    0x07:   OneRegisterDecoder(name='ld', src=DT),
    0x0a:   OneRegisterDecoder(name='ld', src=K),
    0x15:   OneRegisterDecoder(name='ld', dst=DT),
    0x18:   OneRegisterDecoder(name='ld', dst=ST),
    0x1e:   OneRegisterDecoder(name='add', dst=I),
    0x29:   OneRegisterDecoder(name='ld', dst=F),
    0x33:   OneRegisterDecoder(name='ld', dst=B),
    0x55:   OneRegisterDecoder(name='ld', memdst=I),
    0x65:   OneRegisterDecoder(name='ld', memsrc=I),
})

DECODERS = FixedSizeDispatcher(bits=(12, 16), match={
    0x0:    ZeroDecoder(),
    0x1:    AddressDecoder(name='jp', flow_type=JUMP),
    0x2:    AddressDecoder(name='call', flow_type=CALL),
    0x3:    OneRegisterDecoder(name='se', skip=True, const=True),
    0x4:    OneRegisterDecoder(name='sne', skip=True, const=True),
    0x5:    TwoRegistersDecoder(name='se', skip=True),
    0x6:    OneRegisterDecoder(name='ld', const=True),
    0x7:    OneRegisterDecoder(name='add', const=True),
    0x8:    ARITHM_DECODERS,
    0x9:    TwoRegistersDecoder(name='sne'),
    0xa:    ConstantDecoder(name='ld', dst=I),
    0xb:    AddressDecoder(
                name='jp',
                reg=REGISTERS[0], addr=IMMEDIATE, flow_type=JUMP
            ),
    0xc:    OneRegisterDecoder(name='rnd', const=True),
    0xd:    TwoRegistersDecoder(name='drw', const=True),
    0xe:    INPUT_DECODERS,
    0xf:    SPECIAL_DECODERS,
})

class Disassembler(RecursiveDisassembler):
    name = "Chip-8"
    variants = [
        "chip8",
    ]

    registers = REGISTERS
    opcode = struct.Struct(">H")

    def decode(self):
        pc = self.pc
        opcode = BitField(self.opcode.unpack(self.read(2))[0])
        decoder = DECODERS.dispatch(opcode)
        if decoder is None:
            return None
        return decoder(opcode, pc)
