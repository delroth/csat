from collections import defaultdict, deque
from csat.disas.utils import *



class BaseDisassembler:
    """Base class of a disassembler."""

    # Pretty name for the source architecture.
    name = None
    # List of handled architectures (short names).
    variants = None
    # Number of delay slots.
    delay_slots = 0

    def __init__(self, db):
        super(BaseDisassembler, self).__init__()
        self.db = db

    def seek(self, offset):
        """Put the reading cursor at the given offset in the virtual memory."""
        self.pc = offset

    def read(self, size):
        """Read 'size' bytes from the memory and increment the PC."""
        result = self.db.vmem[self.pc:self.pc + size]
        self.pc += size
        return result

    def disassemble(self):
        """
        Disassemble automatically as much code as possible.

        Must be defined by subclasses. No disassembly method is imposed since
        some disassemblers may start at the entry point whereas some others may
        simply disassemble every segment.
        """
        raise NotImplementedError()

    def decode(self):
        """Subclasses must override this method to decode the next
        instruction using the 'read' method."""
        raise NotImplementedError()



class SegmentDisassembler(BaseDisassembler):
    """Linearly disassemble segments. Make basic blocks afterwards."""

    def disassemble(self):
        for segment in self.db.segs.segments:
            self.decode_segment(segment)

        # TODO: make basic blocks

    def decode_segment(self, seg):
        """Decode as much instructions as possible from the given segment."""
        self.pc = seg.address
        while True:
            try:
                instr = self.decode()
            except IndexError:
                # Here, decoding ran out of the segment: we have to stop.
                break
            if instr:
                seg.instrs.append(instr)
            else:
                # Otherwise, the instruction is invalid and thus cannot be
                # decoded. We do not know where the next instruction begins,
                # thus we stop.
                break



class RecursiveDisassembler(SegmentDisassembler):
    """Start from the entry point and disassemble each branch."""

    def disassemble(self):
        self.bblks = self.db.bblks

        # Set of addresses that must be used to disassemble next basic blocks.
        self.todo_list = set()

        # Mapping address -> [n -> [jump addresses]] where n is the number of
        # instructions to execute before jump (because of the delay slot).
        self.pending_delay_slots = defaultdict(lambda: defaultdict(set))

        self.todo_list.add(self.db.entry_point.address)
        while self.todo_list:
            self.disassemble_bblk(self.todo_list.pop())

    def _filter_bblks(self, lower_address, bblks):
        """
        Remove basic blocks that are before the given lower address from bblks.
        """
        while (
            len(bblks) >= 1 and
            bblks[0].address + bblks[0].size < lower_address
        ):
            bblks.popleft()

    def _finalize_bblk(self, bblk, next_addresses=set()):
        """
        Add a basic block and complete its successors if the instruction list
        is not empty.
        """
        if bblk.instrs:
            result = bblk._replace(
                size=sum(instr.size for instr in bblk.instrs)
            )
            bblk.next.update(next_addresses)
            self.bblks[bblk.address] = result
            return result

    def _split_bblk(self, bblk, instr_offset):
        """
        Split a basic block in two parts, the second one starting with the
        'instr_offset'th instruction. Return the lower and the upper basic
        blocks.
        If the instr_offset is just before the first instruction, the returned
        lower basic block is None.
        If the instr_offset is just after the last instruction, the returned
        upper basic block is None.
        """
        if instr_offset >= len(bblk.instrs):
            return (bblk, None)
        if instr_offset <= 0:
            return (None, bblk)

        bblk = self.bblks.pop(bblk.address)
        upper_first_instr = bblk.instrs[instr_offset]
        lower_bblk = BasicBlock(
            bblk.address, 0, bblk.instrs[:instr_offset],
            set(), {upper_first_instr.address}
        )
        upper_bblk = BasicBlock(
            upper_first_instr.address, 0, bblk.instrs[instr_offset:],
            set(), bblk.next
        )
        return (
            self._finalize_bblk(lower_bblk),
            self._finalize_bblk(upper_bblk)
        )

    def _reuse_bblks(self, next_bblks, current_bblk):
        """
        Try to find and to use an already existing basic block for the next
        instruction to fetch. Return if such a basic block was found.
        """
        for bblk in next_bblks:
            if bblk.address > self.pc:
                # There is no already existing basic block that fit the current
                # execution point.
                return False

            if bblk.address == self.pc:
                # The next instruction is the beginning of an already existing
                # basic block.
                current_bblk.next.add(self.pc)
                self._finalize_bblk(current_bblk)
                return True

            # Let's find an instruction that is the next one to be decoded.
            for i, instr in enumerate(bblk.instrs[1:], 1):
                if instr.address == self.pc:
                    # We found it in the middle of a basic bloc: let's split
                    # it.
                    lower_bblk, upper_bblk = self._split_bblk(bblk, i)
                    self._finalize_bblk(current_bblk, {upper_bblk.address})
                    return True

        return False

    def _handle_delay_slots(self, current_block, delay_slots):
        """
        Decrement "active" delay slots. If one delay slot finishes, update the
        "todo_list". Return (new delay_slots, jump addresses).
        """
        new_delay_slots = defaultdict(set)
        next_addresses = set()

        for slots, jump_addresses in delay_slots.items():
            if slots == 0:
                # Build the next possible destination addresses resolving
                # CONTINUE special addresses.
                next_addresses.update(
                    self.pc if addr == CONTINUE else addr
                    for addr in jump_addresses
                )
            else:
                new_delay_slots[slots - 1] = jump_addresses

        if next_addresses:
            self.todo_list.update(
                next_addr for next_addr in next_addresses
                if next_addr != UNKNOWN
            )
            current_block.next.update(next_addresses)
        return (new_delay_slots, next_addresses)

    def _split_delay_slot(self, bblk):
        '''
        Split an existing basic block if it has delay slots pending on it.
        '''
        try:
            delay_slots = self.pending_delay_slots.pop(bblk.address)
        except KeyError:
            return

        for i, instr in enumerate(bblk.instrs, i):
            delay_slots, jump_addresses = self._handle_delay_slots(
                b, delay_slots
            )
            if jump_addresses:
                lower_bblk, upper_bblk = self._split_bblk(bblk, i + 1)
                bblk = lower_bblk or upper_bblk
                bblk.next.update(jump_addresses)
                if delay_slots:
                    next_address = instr.address + instr.size
                    self.todo_list.add(next_address)
                    self.pending_delay_slots[next_address].update(delay_slots)
                return

    def disassemble_bblk(self, entry_point):
        """
        Disassemble as many instructions as possible until reaching a basic
        block boundary.
        """
        # The given entry point may have already been disassembled as the
        # beginning of a basic block.
        try:
            current_bblk = self.bblks[entry_point]
        except KeyError:
            pass
        else:
            return self._split_delay_slot(current_bblk)

        next_bblks = deque(
            self.bblks[address]
            for address in sorted(self.bblks)
        )
        b = BasicBlock(entry_point, 0, [], set(), set())
        delay_slots = self.pending_delay_slots.pop(entry_point, defaultdict(set))

        # TODO: complete the basic block 'prev' field.
        self.seek(entry_point)
        while True:
            self._filter_bblks(self.pc, next_bblks)

            # If the next instruction has already been disassembled, use/split
            # the corresponding basic block.
            if next_bblks and self._reuse_bblks(next_bblks, b):
                if delay_slots:
                    self.pending_delay_slots[entry_point].update(delay_slots)
                    self.todo_list.add(self.pc)
                return

            try:
                instr = self.decode()
            except IndexError:
                # We tried to fetch an instruction outside any mapped memory.
                break
            if instr is None:
                break
            b.instrs.append(instr)

            # If the instruction can control the execution flow, stop the
            # current basic block after it and add appropriate links.
            if instr.flow_type == RETURN:
                delay_slots[self.delay_slots].update(())
                break

            elif instr.flow_type == JUMP:
                dest = instr.flow_dest
                # TODO: try to guess the value(s) the register can contain.
                dest = dest.value if dest.type == CODE_NEAR else UNKNOWN
                delay_slots[self.delay_slots].add(dest)

            elif instr.flow_type in (BRANCH, LOOP):
                dest = instr.flow_dest
                # TODO: try to guess the value(s) the register can contain.
                dest = dest.value if dest.type == CODE_NEAR else UNKNOWN
                delay_slots[self.delay_slots].update((dest, CONTINUE))

            elif instr.flow_type == CALL:
                dest = instr.flow_dest
                if dest.type == CODE_NEAR:
                    self.todo_list.add(dest.value)

            delay_slots, jump_addresses = self._handle_delay_slots(
                b, delay_slots
            )
            if jump_addresses:
                # One delay slot is over: add the other ones to the pending
                # delay slots and stop the current basic block.
                if delay_slots:
                    self.pending_delay_slots[entry_point].update(delay_slots)
                break

        return self._finalize_bblk(b)
