from csat.db.segs import Segments
from csat.db.utils import indent, LITTLE_ENDIAN
from csat.db.vmem import VMemory
from csat.disas.utils import *
import math

class Database:

    def __init__(self):
        self.vmem = VMemory()
        self.segs = Segments(self.vmem)
        self.bblks = {}
        self.labels = {}
        self.label_to_addr = {}
        self.code_ptr_size = 64

    def set_label(self, name, addr):
        self.labels[addr] = name
        self.label_to_addr[name] = addr

    def del_label(self, name):
        for addr, label in self.labels.items():
            if label == name:
                self.labels.pop(addr)
                return True
        self.label_to_addr.pop(name)
        return False

    def fmt_addr(self, offset, label=True):
        if label:
            try:
                return self.labels[offset]
            except KeyError:
                pass
        numeric_fmt = (
            '{:#0' + str(math.ceil(self.code_ptr_size / 4)) + 'x}'
        )
        return numeric_fmt.format(offset)

    def __str__(self):
        return self.format()

    def format(self, include_bblks=True):
        lines = (
            'Entry point:', self.fmt_addr(self.entry_point.address),
            'Endianess: {}'.format(
                'little-endian'
                if self.endianess == LITTLE_ENDIAN else
                'big-endian'
            ),
            'Architecture: {}'.format(self.architecture),
            'Virtual memory:',
            indent(str(self.vmem)),
            'Segments:',
            indent(str(self.segs)),
        )
        if include_bblks:
            lines += (
                'Basic blocks:',
                indent(self._str_bblks(self.bblks)),
            )
        return '\n'.join(lines)

    def find_bblks(self, addr):
        return {
            bblk.address: bblk
            for bblk in self.bblks.values()
            if bblk.address <= addr and bblk.address + bblk.size > addr
        }

    def _str_bblks(self, bblks):
        lines = []
        addresses = sorted(bblks)
        for bblk in (bblks[address] for address in addresses):
            for instr in bblk.instrs:
                lines.extend(repr_instr_full(instr, self))
            next_addresses = [
                (self.fmt_addr(addr) if addr != UNKNOWN else '???')
                for addr in bblk.next
            ]
            lines.append('  -> {}'.format(', '.join(next_addresses)))
            lines.append('')
        return '\n'.join(lines)
