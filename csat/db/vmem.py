class OverlapError(Exception):
    pass

class VMemory:
    """Gather mapped memory."""

    def __init__(self):
        self.mapping = []

    def __str__(self):
        return '\n'.join(
            '{:#016x}-{:#016x} {} bytes'.format(offset, offset + len(bytes), len(bytes))
            for offset, bytes in self.mapping
        )

    def insert(self, bytes, offset):
        """Insert a block of bytes into the virtual memory space. Raise an
        OverlapError if it collides with a previously inserted block."""

        if len(bytes) == 0:
            return

        i = 0
        for i, (block_offset, block_bytes) in enumerate(self.mapping):
            # If we haven't reached the block immediately after the one to
            # insert, check against overlap.
            if block_offset < offset:
                if block_offset + len(block_bytes) > offset:
                    raise OverlapError()
            elif block_offset == offset:
                raise OverlapError()

            # When this block has been reached, check again for overlap.
            elif offset + len(bytes) > block_offset:
                raise OverlapError()
            else:
                break
        else:
            # If we reached the end of the list, we want to insert the new
            # block *past* the end of the list.
            i += 1

        # ... And then actually insert the given block.
        self.mapping.insert(i, (offset, bytes))

    def __getitem__(self, range):
        """Return the content of a range of bytes in the virtual memory."""

        if isinstance(range, int):
            range = slice(range, range + 1)
        if range.step not in (None, 1):
            raise ValueError('Only contiguous bytes can be fetched')

        last_addr = None

        for block_offset, block_bytes in self.mapping:
            block_end = block_offset + len(block_bytes)

            if last_addr is None:
                # We have not met a matching block yet.
                if range.start < block_offset:
                    raise IndexError('Byte range out of any mapped block')

                if range.start >= block_end:
                    continue
                if range.stop > block_end:
                    # The byte range is crossing the block boundary: we'll
                    # check the next blocks.
                    last_addr = block_end
                    continue

                start = range.start - block_offset
                end = range.stop - block_offset
                return block_bytes[start:end]

            else:
                # We have met the first concerned block, but the accessed
                # memory range is too large.
                if last_addr != block_offset:
                    # There must be no gap between this block and the previous
                    # one.
                    raise IndexError('Byte range crossing a block boundary')
                elif range.stop > block_end:
                    # Continue to check against the next block.
                    last_addr = block_end
                    continue

        if last_addr is None:
            raise IndexError('Byte range out of any mapped block')
        else:
            raise IndexError('Byte range cross the last block boundary')
