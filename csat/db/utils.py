class LinearAddr:
    def __init__(self, address):
        self.address = address

class Addr(LinearAddr):
    def __init__(self, segment, offset):
        super(Addr, self).__init__(
            segment.address + offset
        )

LITTLE_ENDIAN, BIG_ENDIAN = range(2)

def indent(lines, level=1, identation='  '):
    """Indent the given lines 'level' times."""
    prefix = identation * level
    return '\n'.join('{}{}'.format(prefix, line) for line in lines.split('\n'))
