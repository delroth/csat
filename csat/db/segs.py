from collections import namedtuple
from csat.db.utils import indent
from csat.disas.utils import repr_instr_full

Segment = namedtuple('Segment', 'name address size instrs')

class Segments:

    def __init__(self, vmem):
        self.vmem = vmem
        self.segments = []

    def add(self, name, address, size):
        result = Segment(name, address, size, [])
        self.segments.append(result)
        return result

    def __str__(self):
        lines = []
        for name, addr, size, instrs in self.segments:
            lines.append('{:#016x}-{:#016x} {}'.format(
                addr, addr+size, name
            ))
            for instr in instrs:
                lines.append(indent(
                    '\n'.join(repr_instr_full(instr, self.vmem))
                ))
        return '\n'.join(lines)

    def __iter__(self):
        return iter(self.segments)
