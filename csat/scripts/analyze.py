#! /usr/bin/env python3

import csat.db
import csat.ui.extensions as extensions
import sys

def make_db():
    binary_loaders = extensions.get_binfmt()
    disassemblers = extensions.get_disas()

    db = csat.db.Database()
    try:
        filename = sys.argv[1]
        f = open(filename, 'rb')
    except IndexError:
        print('Usage: {} binfile'.format(sys.argv[0]), file=sys.stderr)
        sys.exit(2)
    except (OSError, IOError) as e:
        print('Cannot open {}: {}'.format(sys.argv[1], e))
        sys.exit(1)

    try:
        labels = sys.argv[2]
    except IndexError:
        pass
    else:
        for assoc in labels.split(','):
            try:
                label, address = assoc.split('=')
                address = int(address, 16)
            except ValueError:
                continue
            else:
                db.labels[address] = label

    handled = False
    for BinaryLoader in binary_loaders:
        binfmt = BinaryLoader()
        f.seek(0)
        if binfmt.accept(f, filename):
            handled = True
            f.seek(0)
            binfmt.load(f, db)
            break
    if not handled:
        print('Could not determine the architecture format.')
        sys.exit(1)

    arch = db.architecture
    if db.architecture is not None:
        handled = False
        for Disassembler in disassemblers:
            if arch in Disassembler.variants:
                handled = True
                Disassembler(db).disassemble()
        if not handled:
            print('Could not find a disassembler for architecture', arch)
    else:
        print(
            'There is no architecture hint: '
            'cannot get the proper disassembler.'
        )
        sys.exit(1)

    return db

def main():
    print(str(make_db()))



if __name__ == '__main__':
    main()
