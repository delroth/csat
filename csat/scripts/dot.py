#! /usr/bin/env python3

import csat.db
from csat.disas.utils import repr_instr
from csat.scripts.analyze import make_db
import csat.ui.extensions as extensions
import sys

def main():
    db = make_db()
    print('digraph G {')
    for addr, bblk in db.bblks.items():
        print('{} [shape=box, label="{}\\l\\l{}", labeljust=l]'.format(
            addr,
            db.fmt_addr(addr),
            ''.join(repr_instr(instr, db) + '\\l' for instr in bblk.instrs)
        ))
        for next_addr in bblk.next:
            print('{} -> {} [headport=n, tailport=s]\n'.format(addr, next_addr))
    print('}')



if __name__ == '__main__':
    main()
