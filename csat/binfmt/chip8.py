from csat.db.utils import Addr, BIG_ENDIAN


class BinaryLoader:

    name = 'Chip-8 ROM'
    variants = ['chip8']

    def accept(self, fp, filename):
        '''
        Only check the file extension. There is no particular magic for a
        Chip-8 ROM.
        '''
        return filename.endswith('.chip8')

    def load(self, fp, db):
        '''Load the file into the csat database.'''
        bytes = fp.read()

        db.vmem.insert(bytes, 0x200)
        rom = db.segs.add('rom', 0x0200, len(bytes))
        db.entry_point = Addr(rom, 0x000)
        db.architecture = 'chip8'
        db.endianess = BIG_ENDIAN
        db.code_ptr_size = 12
