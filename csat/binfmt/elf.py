from collections import namedtuple
from csat.db.utils import LinearAddr, LITTLE_ENDIAN, BIG_ENDIAN
import mmap
from struct import Struct

elf_ident_size = 16
elf_magic = b'\x7fELF'
elf_class = {
    'none': 0,
    '32': 1,
    '64': 2,
}

elf_data = {
    'none': 0,
    '2LSB': 1,
    '2MSB': 2,
}

elf_version_offset = 6
elf_version = 1

elf_osabi = {
    'none':         0,
    'sysv':         0,
    'hpux':         1,
    'netbsd':       2,
    'gnu':          3,
    'linux':        3,
    'solaris':      6,
    'aix':          7,
    'irix':         8,
    'freebsd':      9,
    'tru64':        10,
    'modesto':      11,
    'openbsd':      12,
    'arm_aeabi':    64,
    'arm':          97,
    'standalone':   255,
}

elf_abiversion = 0

elf_type = {
    'none': 0,
    'rel':  1,
    'exec': 2,
    'dyn':  3,
    'core': 4,
}

elf_machine = {
    'none':         0,
    'm32':          1,
    'sparc':        2,
    '386':          3,
    '68k':          4,
    '88k':          5,
    '860':          7,
    'mips':         8,
    's370':         9,
    'mips_rs3_le':  10,
    'parisc':       15,
    'vpp500':       17,
    'sparc32plus':  18,
    '960':          19,
    'ppc':          20,
    'ppc64':        21,
    's390':         22,
    'v800':         36,
    'fr20':         37,
    'rh32':         38,
    'x86_64':       62,
    # ... TODO
}

architectures = {}
for name, id in elf_machine.items():
    architectures[id] = name

phdr_type = {
    'null':     0,
    'load':     1,
    'dynamic':  2,
    'interp':   2,
    'note':     2,
    'shlib':    2,
    'phdr':     2,
    'tls':      2,
}

shdr_type = {
    'null':     0,
    'progbits': 1,
    # TODO: complete me
}

shdr_flags = {
    'write':        1,
    'alloc':        2,
    'execinstr':    4,
    # TODO: complete me
}


field_tables = {
    elf_class['32']: {
        'h': 'H',           # Half words are unsigned 16 bits
        'w': 'i', 'W': 'I', # Words are 32 bits
        'x': 'q', 'X': 'Q', # Extended words are 64 bits
        'p': 'I',           # Pointers are unsigned 32 bits
        'o': 'I',           # File offsets are unsigned 32 bits
        'S': 'H',           # Section numbers are unsigned 16 bits
    },
    elf_class['64']: {
        'h': 'H',           # Half words are unsigned 16 bits
        'w': 'i', 'W': 'I', # Words are 32 bits
        'x': 'q', 'X': 'Q', # Extended words are 64 bits
        'p': 'Q',           # Pointers are unsigned 64 bits
        'o': 'Q',           # File offsets are unsigned 64 bits
        'S': 'H',           # Section numbers are unsigned 16 bits
    },
}

elf_hdr_format = '16shhWpooW6h'
elf_hdr_tuple = namedtuple(
    'Hdr',
    'magic type machine version '
    'entry phoff shoff flags ehsize phentsize phnum shentsize shnum shstrndx'
)
phdr_formats = {
    elf_class['32']: 'Wopp4W',
    elf_class['64']: 'WWopp3X',
}
phdr_tuples = {
    elf_class['32']: namedtuple(
        'Phdr32', 'type offset vaddr paddr filesz memsz flags align'
    ),
    elf_class['64']: namedtuple(
        'Phdr64', 'type flags offset vaddr paddr filesz memsz align'
    ),
}

shdr_formats = {
    elf_class['32']: '3Wpo5W',
    elf_class['64']: 'WWXpoXWWXX',
}
shdr_tuples = {
    elf_class['32']: namedtuple(
        'Shdr32',
        'name type flags addr offset size link info addralign entsize'
    ),
    elf_class['64']: namedtuple(
        'Shdr64',
        'name type flags addr offset size link info addralign entsize'
    ),
}

class BinaryLoader:

    name = 'ELF'
    variants = ['elf', 'elf32', 'elf64']

    def get_struct(self, elf_info, format):
        '''Return a Struct using the proper byte order and class.'''

        field_table = field_tables[elf_info['class']]
        byte_order = '<' if elf_info['data'] == elf_data['2LSB'] else '>'

        return Struct('{}{}'.format(
            byte_order,
            ''.join(field_table.get(field, field) for field in format)
        ))

    def get_elf_info(self, map):
        '''
        Extract general information on the given ELF. Return None if this is
        not a valid ELF, otherwise return a dictionary.
        '''

        info = {}
        ident = map[:elf_ident_size]
        if len(ident) != elf_ident_size or ident[:4] != elf_magic:
            return None

        info.update({
            'class': ident[4],
            'data': ident[5],
            'version': ident[6],
            'osabi': ident[7],
            'abiversion': ident[8],
        })

        if not all((
            info['class'] in (elf_class['32'], elf_class['64']),
            info['data'] in (elf_data['2LSB'], elf_data['2MSB']),
            info['version'] == elf_version,
            info['abiversion'] == elf_abiversion,
        )):
            return None

        struct = self.get_struct(info, elf_hdr_format)
        elf_hdr = elf_hdr_tuple(*struct.unpack(map[:struct.size]))

        info.update({
            'type':         elf_hdr.type,
            'machine':      elf_hdr.machine,
            'version':      elf_hdr.version,
            'entry':        elf_hdr.entry,
            'phoff':        elf_hdr.phoff,
            'shoff':        elf_hdr.shoff,
            'flags':        elf_hdr.flags,
            'ehsize':       elf_hdr.ehsize,
            'phentsize':    elf_hdr.phentsize,
            'phnum':        elf_hdr.phnum,
            'shentsize':    elf_hdr.shentsize,
            'shnum':        elf_hdr.shnum,
            'shstrndx':     elf_hdr.shstrndx,
        })

        return info


    def accept(self, fp, filename):
        """Check if the file is a valid executable ELF."""
        with mmap.mmap(fp.fileno(), 0, access=mmap.ACCESS_READ) as map:
            info = self.get_elf_info(map)
            return info is not None and info['type'] == elf_type['exec']


    def load(self, fp, db):
        with mmap.mmap(fp.fileno(), 0, access=mmap.ACCESS_READ) as map:
            info = self.get_elf_info(map)

            phdr_struct = self.get_struct(info, phdr_formats[info['class']])
            shdr_struct = self.get_struct(info, shdr_formats[info['class']])
            phdr_tuple = phdr_tuples[info['class']]
            shdr_tuple = shdr_tuples[info['class']]

            def get_phdr(i):
                phdr_offset = info['phoff'] + i * info['phentsize']
                return phdr_tuple(*phdr_struct.unpack(
                    map[phdr_offset:phdr_offset + info['phentsize']]
                ))

            def get_shdr(i):
                shdr_offset = info['shoff'] + i * info['shentsize']
                return shdr_tuple(*shdr_struct.unpack(
                    map[shdr_offset:shdr_offset + info['shentsize']]
                ))

            for i in range(info['phnum']):
                phdr = get_phdr(i)
                if phdr.type == phdr_type['load']:
                    bytes = (
                        map[phdr.offset:phdr.offset + phdr.filesz] +
                        b'\x00' * (phdr.memsz - phdr.filesz)
                    )
                    db.vmem.insert(bytes, phdr.vaddr)

            # Get the string table section.
            str_shdr = get_shdr(info['shstrndx'])
            str_table = map[str_shdr.offset:str_shdr.offset + str_shdr.size]
            def get_name(index):
                string_end = str_table.find(b'\x00', index)
                return str_table[index:string_end].decode('ascii')

            for i in range(info['shnum']):
                shdr = get_shdr(i)
                if shdr.type != shdr_type['progbits']:
                    continue
                if shdr.flags & shdr_flags['execinstr']:
                    db.segs.add(get_name(shdr.name), shdr.addr, shdr.size)

            db.entry_point = LinearAddr(info['entry'])
            db.endianess = (
                LITTLE_ENDIAN
                if info['type'] == elf_data['2LSB'] else
                BIG_ENDIAN
            )
            db.architecture = architectures.get(info['machine'], None)
            db.code_ptr_size = 32 if info['class'] == elf_class['32'] else 64
