from csat.db.utils import Addr


class BinaryLoader:
    name = 'Gameboy ROM'
    variants = ['gb', 'gbrom']

    def accept(self, fp, filename):
        """Check if the file is a valid Gameboy ROM image."""
        fp.seek(0x104)
        magic = fp.read(4)

        return magic == b"\xCE\xED\x66\x66"


    def load(self, fp, db):
        """Load the file into the csat database."""
        bytes = fp.read()

        db.vmem.insert(bytes, 0x0000)
        rom = db.segs.add("rom", 0x0000, len(bytes))
        db.entry_point = Addr(rom, 0x100)
        db.architecture = "z80"
        db.code_ptr_size = 16
