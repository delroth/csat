from csat.db.utils import LinearAddr, LITTLE_ENDIAN
from struct import Struct


sector_size = 2048

header = Struct(
    "<8s"  # Magic
    "I"   # Text segment offset
    "I"   # Data segment offset
    "I"   # Entry point
    "I"   # Initial stack location

    "II"  # Text segment (addr, size)
    "II"  # Data segment (addr, size)
    "II"  # BSS segment (addr, size)
    "II"  # Stack segment (addr, size)
)


class BinaryLoader:

    name = 'PSX Executable'
    variants = ['psx', 'psxexe']

    def accept(self, fp, filename):
        """Check if the file is a valid PSX executable image."""
        return fp.read(8) == b"PS-X EXE"

    def load(self, fp, db):
        """Load the file into the csat database."""
        hdr_bytes = fp.read(header.size)
        hdr = header.unpack(hdr_bytes)

        text_offset = hdr[1]
        data_offset = hdr[2]
        entry_point = hdr[3]
        initial_stack = hdr[4]
        text_addr, text_size = hdr[5:7]
        data_addr, data_size = hdr[7:9]
        bss_addr, bss_size = hdr[9:11]
        stack_addr, stack_size = hdr[11:13]

        fp.seek(sector_size + text_offset)
        db.vmem.insert(fp.read(text_size), text_addr)

        fp.seek(sector_size + data_offset)
        db.vmem.insert(fp.read(data_size), data_addr)

        db.segs.add("text", text_addr, text_size)
        db.segs.add("data", data_addr, data_size)
        db.segs.add("bss", bss_addr, bss_size)
        db.segs.add("stack", stack_addr, stack_size)

        db.entry_point = LinearAddr(entry_point)
        db.endianess = LITTLE_ENDIAN
        db.architecture = "mips_r3000"
        db.code_ptr_size = 32
